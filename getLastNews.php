<?php
	/**
	* Class rssDigest
	*/
	class rssDigest
	{
		public $url;
		public $limit;

		/**
		* rssDigest конструктор
		*/	
		function __construct($url,$limit) {
			$this->url = $url;
			$this->limit = $limit;
		}
		
		/**
		* Получение последних статей из rss, ограниченое по количеству
		*/
		public function printDigest ()
		{
			$xml = simplexml_load_file($this->url, "SimpleXMLElement", LIBXML_NOCDATA);
			if ($xml) {
				$items = array_slice($xml->xpath('//item'), 0 - $this->limit, $this->limit);
				foreach($items as $item) {
					echo 'Название: '.(string)$item->title.PHP_EOL;
					echo 'Ссылка: '.(string)$item->link.PHP_EOL;
					echo 'Анонс: '.trim((string)$item->description).PHP_EOL;
				}
			}	
		}
	}

	$rss = new rssDigest("https://habr.com/ru/rss/best/daily/", 5);
	$rss->printDigest();
?>