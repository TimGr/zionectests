<?php
	require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
	use Bitrix\Main\Loader;
	use \Bitrix\Main\Data\Cache;
	
	/**
	* Class iBlockCache
	*/
	class iBlockCache
	{
		private $cacheTime;
		private $cacheDir;

		/**
		* iBlockCache конструктор.
		*/
		public function __construct($cacheTime = 3600, $cacheDir = '/iBlockCache')
		{
			$this->cacheTime = $cacheTime;
			$this->cacheDir = $cacheDir;
		}
		
		/**
		* функция получения данных из ИБ с учетом кеширования
		*/
		public function getElementsList( $arOrder = array("SORT"=>"ASC"),  $arFilter = array(),  $arSelectFields = array())
		{
			if (\Bitrix\Main\Loader::includeModule('iblock')) {
				$cache = \Bitrix\Main\Data\Cache::createInstance();
				
				//формируем уникальный id зависящий от параметров выборки
				$cacheId = md5(json_encode(array($sort,$filter,$select)));
				
				if ($cache->initCache($this->cacheTime, $cacheId,  $this->cacheDir)) {
					//кеш еще валидный, используем.
					$result = $cache->getVars();
				} elseif ($cache->startDataCache()) {
					//кеш невалидный/несуществует, исполняем код и....
					$result = array();
					$res = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelectFields);
					while ($arFields = $res->GetNext()) {
						$result[] = $arFields;
					}
					
					if (count($result) == 0){
						//...отменяем кеш - нечего кешировать.
						$cache->abortDataCache();
					} else {
						//...кладем в кеш.
						$cache->endDataCache($result);
					}
				}
				return $result;
			} else {
				return false;
			}
		}
	}
?>